[ -z "$BASH_VERSION" ] && return
# always run interactively
[ -z "$PS1" ] && return
# set nvim as manpager
command -v nvim > /dev/null 2>&1 && export MANPAGER="nvim '+set ft=man' -"
stty -ixon
#---------------------------------------------------------------
# Options
# --------------------------------------------------------------
# History
# =========
HISTFILE=/home/$USER/.local/share/bash_history
HISTCONTROL=erasedups:ignoreboth
HISTSIZE=20000
HISTFILESIZE=20000
HISTIGNORE='exit:cd:ls:bg:fg:history:clear:c'
HISTTIMEFORMAT='%F %T'
shopt -s checkwinsize
shopt -s checkhash # check command line
shopt -s histappend
shopt -s cmdhist
shopt -s extglob
shopt -s globstar 2>/dev/null
shopt -s autocd 2>/dev/null
shopt -s dirspell 2>/dev/null
shopt -s cdspell 2>/dev/null
# append hist file after each command
PROMPT_COMMAND='history -a'
# truncate long paths
PROMPT_DIRTRIM=4

#---------------------------------------------------------------
# Completion
#---------------------------------------------------------------
# [ -f /etc/bash_completion ] && . /etc/bash_completion
[ -f /usr/share/bash_completion/bash_completion ] && \
    . /usr/share/bash-completion/bash_completion

#---------------------------------------------------------------
# External
#---------------------------------------------------------------
# git prompt
[ -f /usr/lib/git-core/git-sh-prompt ] && . /usr/lib/git-core/git-sh-prompt
# sh.d
for r in "$XDG_CONFIG_HOME"/sh.d/{aliases,functions}; do
    source "$r"
done
# FZF
[ -f "$HOME"/.fzf.bash ] && source "$HOME"/.fzf.bash
#---------------------------------------------------------------
# Prompt
#---------------------------------------------------------------
GIT_PS1_SHOWDIRTYSTATE=true
GIT_PS1_SHOWUNTRACKEDFILES=true
PS1='$(__git_ps1 " %s") \[\e[0;32m\]@\h \[\e[1;35m\]> \[\e[0m\]'
# PS1='$(bashy) \[\e[1:33m\]❯ \[\e[0m\]'
