
" denite.nvim
" -----------

" INTERFACE
call denite#custom#option('_', {
	\ 'prompt': '>',
	\ 'empty': 0,
	\ 'winheight': 16,
	\ 'source_names': 'short',
	\ 'vertical_preview': 1,
	\ 'auto-accel': 1,
	\ 'auto-resume': 1,
	\ })

call denite#custom#option('list', {})

" MATCHERS
" Default is 'matcher_fuzzy'
call denite#custom#source('tag', 'matchers', ['matcher_substring'])
if has('nvim') && &runtimepath =~# '\/cpsm'
	call denite#custom#source(
		\ 'buffer,file_mru,file_old,file_rec,grep,line',
		\ 'matchers', ['matcher_fuzzy', 'matcher_cpsm'])
endif

" SORTERS
" Default is 'sorter_rank'
call denite#custom#source('z', 'sorters', ['sorter_z'])

" CONVERTERS
" Default is none
call denite#custom#source(
	\ 'buffer,file_mru,file_old',
	\ 'converters', ['converter_relative_word'])

" FIND and GREP COMMANDS
if executable('ag')
	" The Silver Searcher
	call denite#custom#var('file_rec', 'command',
		\ ['ag', '-U', '-H', '--hidden', '--follow',
		\ '--nocolor', '--nogroup', '-g', ''])

	" Setup ignore patterns in your .agignore file!
	" https://github.com/ggreer/the_silver_searcher/wiki/Advanced-Usage

	call denite#custom#var('grep', 'command', ['ag'])
	call denite#custom#var('grep', 'recursive_opts', [])
	call denite#custom#var('grep', 'pattern_opt', [])
	call denite#custom#var('grep', 'separator', ['--'])
	call denite#custom#var('grep', 'final_opts', [])
	call denite#custom#var('grep', 'default_opts',
		\ [ '--skip-vcs-ignores', '--vimgrep', '--smart-case', '--hidden' ])

elseif executable('rg')
	" The Silver Searcher
	call denite#custom#var('file_rec', 'command',
		\ ['rg', '--hidden', '--files', '-L',
		\ '--no-heading', '--glob', '!.git', '--glob', ''])

	call denite#custom#var('grep', 'command', ['rg'])
	call denite#custom#var('grep', 'recursive_opts', [])
	call denite#custom#var('grep', 'pattern_opt', ['--regexp'])
	call denite#custom#var('grep', 'separator', ['--'])
	call denite#custom#var('grep', 'final_opts', [])
	call denite#custom#var('grep', 'default_opts',
		\ [ '--vimgrep', '--no-heading', '--smart-case', '--hidden' ])




elseif executable('ack')
	" Ack command
	call denite#custom#var('grep', 'command', ['ack'])
	call denite#custom#var('grep', 'recursive_opts', [])
	call denite#custom#var('grep', 'pattern_opt', ['--match'])
	call denite#custom#var('grep', 'separator', ['--'])
	call denite#custom#var('grep', 'final_opts', [])
	call denite#custom#var('grep', 'default_opts',
			\ ['--ackrc', $HOME.'/.config/ackrc', '-H',
			\ '--nopager', '--nocolor', '--nogroup', '--column'])
endif

" KEY MAPPINGS
let g:mydenite_insert_mode_mappings = [
    \ ['<C-n>', '<denite:move_to_next_line>', 'noremap'],
    \ ['<C-e>', '<denite:move_to_previous_line>', 'noremap'],
    \ ['<C-j>', '<denite:assign_next_text>', 'noremap'],
    \ ['<C-k>', '<denite:move_caret_to_tail>', 'noremap'],
    \ ['<C-g>', '<denite:insert_digraph>', 'noremap'],
    \ ['<C-t>', '<denite:input_command_line>', 'noremap'],
    \ ['<Esc>', '<denite:enter_mode:normal>', 'noremap'],
    \ ['<C-S-n>', '<denite:assign_next_matched_text>', 'noremap'],
    \ ['<C-P>', '<denite:assign_previous_matched_text>', 'noremap'],
    \ ['<Up>', '<denite:assign_previous_text>', 'noremap'],
    \ ['<Down>', '<denite:assign_next_text>', 'noremap'],
    \ ]

let g:mydenite_normal_mode_mappings = [
    \ ['<C-n>', '<denite:jump_to_next_source>', 'noremap'],
    \ ['<C-p>', '<denite:jump_to_previous_source>', 'noremap'],
    \ ['gg', '<denite:move_to_first_line>', 'noremap'],
    \ ['st', '<denite:do_action:tabopen>', 'noremap'],
    \ ['sg', '<denite:do_action:vsplit>', 'noremap'],
    \ ['sv', '<denite:do_action:split>', 'noremap'],
    \ ['sc', '<denite:quit>', 'noremap'],
    \ ['r', '<denite:redraw>', 'noremap'],
    \ ['j', '<denite:do_action:new>', 'noremap'],
    \ ['k', '<denite:do_action:edit>', 'noremap'],
    \ ['l', '<denite:enter_mode:insert>', 'noremap'],
    \ ['n', '<denite:move_to_next_line>', 'noremap'],
    \ ['e', '<denite:move_to_previous_line>', 'noremap'],
    \ ['<C-w>n', '<denite:wincmd:j>', 'noremap'],
    \ ['<C-w>e', '<denite:wincmd:k>', 'noremap'],
    \ ['<C-w>i', '<denite:wincmd:l>', 'noremap'],
    \]

for m in g:mydenite_insert_mode_mappings
	call denite#custom#map('insert', m[0], m[1], m[2])
endfor
for m in g:mydenite_normal_mode_mappings
	call denite#custom#map('normal', m[0], m[1], m[2])
endfor

" vim: set ts=2 sw=2 tw=80 noet :
