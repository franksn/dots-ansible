
" Langserver
" ---------
" let g:LanguageClient_diagnosticsList = 'Location'
let g:LanguageClient_serverCommands = {
    \ 'rust': ['rustup', 'run', 'nightly', 'rls'],
    \ 'cpp': ['cquery', '--log-file=/tmp/cq.log'],
    \ 'c': ['cquery', '--log-file=/tmp/cq.log'],
    \ 'javascript': ['javascript-typescript-stdio'],
		\ 'jsx': ['javascript-typescript-stdio'],
    \ 'python': ['pyls'],
    \ 'html': ['html-languageserver', '--stdio'],
    \ 'css': ['css-languageserver', '--stdio'],
    \}
let g:LanguageClient_loadSettings = 1

augroup LanguageClientConfig
    autocmd!
    "go to definition
    autocmd FileType c,cpp,javascript,jsx,python,typescript,go,html,rust,css,scss,less
                \ nnoremap <buffer> <leader>mgd :call
                \ LanguageClient_textDocument_definition()<CR>
    autocmd FileType c,cpp,javascript,jsx,python,typescript,go,html,rust,css,scss,less
                \ nnoremap <buffer> <leader>mgK :call
                \ LanguageClient_textDocument_hover()<CR>
    autocmd FileType c,cpp,javascript,jsx,python,typescript,go,html,rust,css,scss,less
                \ nnoremap <buffer> <leader>mgr :call
                \ LanguageClient_textDocument_rename()<CR>
    " autocmd FileType c,cpp,javascript,python,typescript,go,html,rust,css,less,scss
    "             \ setlocal omnifunc=LanguageClient#complete

augroup END

if dein#tap('denite.nvim')
	nnoremap <silent><localleader>mss :<C-u>Denite workspaceSymbol documentSymbol<CR>
	nnoremap <silent><localleader>msd :<C-u>Denite references<CR>
endif

let $RUST_SRC_PATH = "$HOME/.rustup/toolchains/nightly-*/lib/rustlib/src/rust/src"
" vim: set foldmethod=marker ts=2 sw=2 tw=80 noet :
