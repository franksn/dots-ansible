
" Clang Format
" ---------
let g:clang_format#code_style = "google"
let g:clang_format#style_options = {
            \ "AccessModifierOffset" : -1,
            \ "AllowShortIfStatementsOnASingleLine" : "true",
            \ "AlwaysBreakTemplateDeclarations" : "true",
            \ "Standard" : "C++11",
            \ "BreakBeforeBraces" : "Linux"
            \ }

" map to <Leader>m= in C++ code
autocmd FileType c,cpp nnoremap <buffer><Leader>m= :<C-u>ClangFormat<CR>
autocmd FileType c,cpp vnoremap <buffer><Leader>m= :ClangFormat<CR>
" if you install vim-operator-user
autocmd FileType c,cpp,objc map <buffer><LocalLeader>m= <Plug>(operator-clang-format)
autocmd FileType c,cpp ClangFormatAutoEnable

" vim: set foldmethod=marker ts=2 sw=2 tw=80 noet :
