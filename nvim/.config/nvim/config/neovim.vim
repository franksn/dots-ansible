" Write history on idle, for sharing among different sessions
autocmd MyAutoCmd CursorHold * if exists(':rshada') | rshada | wshada | endif

" Search and use environments specifically made for Neovim.
if isdirectory($HOME.'/.pyenv/shims')
	let g:python3_host_prog = $HOME.'/.pyenv/shims/python3'
	let g:python_host_prog = $HOME.'/.pyenv/shims/python2'
endif
