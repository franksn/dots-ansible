" Tao Theme from  Emacs theme of the same name

if version > 580
	hi clear
	if exists("syntax_on")
		syntax reset
	endif
endif

let g:colors_name = "taoyin"
let s:tao_theme_ver = "0.0.1"

let s:gui_bg = "#171717"
let s:gui_bg2 = "#252525"
let s:gui_fg = "#dadada"
let s:gui_black = "#3c3c3c"
let s:gui_grey = "#616161"
let s:gui_red = "#e8e8e8"
let s:gui_green = "#dadada"
let s:gui_yellow = "#f6f6f6"
let s:gui_blue = "#f1f1f1"
let s:gui_magenta = "#c3c3c3"
let s:gui_cyan = "#9e9e9e"
let s:gui_white = "#fafafa"
let s:gui_bright = "#fcfcfc"
let s:gui_num = "#ffaf00"
let s:gui_cursor = "#d7af80"
let s:gui_diff = "#afd7d7"
let s:gui_err = "#d75faf"

let s:c_bg = "236"
let s:c_bg2 = "241"
let s:c_black = "0"
let s:c_grey = "8"
let s:c_red = "1"
let s:c_green = "2"
let s:c_yellow = "3"
let s:c_blue = "4"
let s:c_magenta = "5"
let s:c_cyan = "6"
let s:c_white = "7"
let s:c_bright = "15"
let s:c_num = "214"
let s:c_cursor = "178"
let s:c_diff = "152"
let s:c_err = "169"

if !exists("g:taoyin_italic_comments")
	let g:taoyin_italic_comments = 0
endif

function! s:hi(group, guifg, guibg, ctermfg, ctermbg, attr, guisp)
	let l:attr = a:attr
	if g:taoyin_italic_comments == 0 && l:attr ==? 'italic'
		let l:attr = 'NONE'
	endif
	if a:guifg != ""
		exec "hi " . a:group . " guifg=" . a:guifg
	endif
	if a:guibg != ""
		exec "hi " . a:group . " guibg=" . a:guibg
	endif
	if a:ctermfg != ""
		exec "hi " . a:group . " ctermfg=" . a:ctermfg
	endif
	if a:ctermbg != ""
		exec "hi " . a:group . " ctermbg=" . a:ctermbg
	endif
	if a:attr != ""
		exec "hi " . a:group . " gui=" . l:attr . " cterm=" . l:attr
	endif
	if a:guisp != ""
		exec "hi " . a:group . " guisp=" . a:guisp
	endif
endfunction

"=========================
" Components
"=========================
call s:hi("Bold", "", "", "", "", "bold", "")
call s:hi("Italic", "", "", "", "", "italic", "")
call s:hi("Underline", "", "", "", "", "underline", "")

"+--- Editor ---+
call s:hi("ColorColumn", "", s:gui_bright, "NONE", s:c_bright, "", "")
call s:hi("Cursor", s:gui_num, "", "", "NONE", "", "")
call s:hi("Error", s:gui_err, s:gui_black, "", s:c_err, "", "")
call s:hi("CursorLine", "", "NONE", "", "NONE", "bold", "")
call s:hi("LineNr", s:gui_black, "NONE", s:c_black, "NONE", "", "")
call s:hi("MatchParen", s:gui_black, s:gui_diff, s:c_black, s:c_diff, "bold", "")
call s:hi("NonText", s:gui_grey, "", s:c_grey, "", "", "")
call s:hi("Normal", s:gui_fg, "", "NONE", "NONE", "", "")
call s:hi("PMenu", s:gui_cyan, s:gui_bg, s:c_cyan, s:c_bg, "NONE", "")
call s:hi("PmenuSbar", s:gui_red, s:gui_bg2, "NONE", s:c_green, "", "")
call s:hi("PMenuSel", s:gui_bright, s:gui_black, s:c_bright, s:c_black, "", "")
call s:hi("PmenuThumb", s:gui_bright, s:gui_black, "NONE", s:c_black, "", "")
call s:hi("SpecialKey", s:gui_magenta, "NONE", s:c_magenta, "NONE", "", "")
call s:hi("SpellBad", "", s:gui_err, "", "NONE", "undercurl", s:gui_bright)
call s:hi("SpellCap", "", s:gui_black, "", "NONE", "undercurl", s:gui_magenta)
call s:hi("SpellLocal", "", s:gui_black, "", "NONE", "undercurl", s:gui_blue)
call s:hi("SpellRare", "", s:gui_black, "", "NONE", "undercurl", s:gui_yellow)
call s:hi("Visual", s:gui_bg, s:gui_blue, s:c_bg, s:c_yellow, "", "")
call s:hi("VisualNOS", s:gui_black, s:gui_blue, s:c_bg, s:c_green, "", "")
"+- Neovim Support -+
call s:hi("healthError",   s:gui_bg, s:gui_err, s:c_bg, s:c_err, "", "")
call s:hi("healthSuccess", s:gui_bg, s:gui_diff, s:c_bg, s:c_diff, "", "")
call s:hi("healthWarning", s:gui_bg, s:gui_num, s:c_bg, s:c_num, "", "")

""+- Neovim Terminal Colors -+
if has('nvim')
  let g:terminal_color_0  = s:gui_black
  let g:terminal_color_1  = s:gui_red
  let g:terminal_color_2  = s:gui_green
  let g:terminal_color_3  = s:gui_yellow
  let g:terminal_color_4  = s:gui_blue
  let g:terminal_color_5  = s:gui_magenta
  let g:terminal_color_6  = s:gui_cyan
  let g:terminal_color_7  = s:gui_white
  let g:terminal_color_8  = s:gui_grey
  let g:terminal_color_9  = s:gui_red
  let g:terminal_color_10 = s:gui_grey
  let g:terminal_color_11 = s:gui_yellow
  let g:terminal_color_12 = s:gui_blue
  let g:terminal_color_13 = s:gui_magenta
  let g:terminal_color_14 = s:gui_cyan
  let g:terminal_color_15 = s:gui_bright
endif

"+--- Gutter ---+
call s:hi("CursorLineNr", s:gui_grey, s:gui_bg, "NONE", "", "", "")
call s:hi("Folded", s:gui_bright, s:gui_grey, s:c_bright, s:c_bg2, "bold", "")
call s:hi("FoldColumn", s:gui_cyan, s:gui_bg2, s:c_cyan, "NONE", "", "")
call s:hi("SignColumn", s:gui_red, s:gui_bg2, s:c_cyan, "NONE", "", "")

"+--- Navigation ---+
call s:hi("Directory", s:gui_blue, "", s:c_blue, "NONE", "", "")

"+--- Prompt/Status ---+
call s:hi("EndOfBuffer", s:gui_yellow, "", s:c_yellow, "NONE", "", "")
call s:hi("ErrorMsg", s:gui_err, s:gui_bg2, "NONE", s:c_err, "", "")
call s:hi("ModeMsg", s:gui_cyan, "", s:c_cyan, "", "", "")
call s:hi("MoreMsg", s:gui_green, "", s:c_green, "", "", "")
call s:hi("Question", s:gui_white, "", "NONE", "", "", "")
" call s:hi("StatusLine", s:gui_cyan, s:gui_bg2, s:c_cyan, s:c_bg, "NONE", "")
" call s:hi("StatusLineNC", s:gui_cyan, s:gui_bg2, "NONE", s:c_bg, "NONE", "")
call s:hi("WarningMsg", s:gui_num, s:gui_bg, s:c_bg, s:c_num, "bold", "")
call s:hi("WildMenu", s:gui_bg2, s:gui_blue, s:c_bg, s:c_blue, "", "")

"+--- Search ---+
call s:hi("IncSearch", s:gui_bg, s:gui_yellow, s:c_bg, s:c_yellow, "underline", "")
call s:hi("Search", s:gui_bg, s:gui_yellow, s:c_bg, s:c_yellow, "NONE", "")

""+--- Tabs ---+
call s:hi("TabLine", s:gui_magenta, s:gui_bg2, "NONE", s:c_magenta, "NONE", "")
call s:hi("TabLineFill", s:gui_cyan, s:gui_bg2, "NONE", s:c_cyan, "NONE", "")
call s:hi("TabLineSel", s:gui_red, s:gui_grey, s:c_red, s:c_grey, "NONE", "")

"+--- Window ---+
call s:hi("Title", s:gui_cyan, "", s:c_cyan, "", "NONE", "")
call s:hi("VertSplit", s:gui_black, s:gui_bg2, s:c_grey, "NONE", "NONE", "")

"+----------------------+
"+ Language Base Groups +
"+----------------------+
call s:hi("Boolean", s:gui_blue, "", s:c_blue, "", "", "")
call s:hi("Character", s:gui_green, "", s:c_green, "", "", "")
call s:hi("Comment", s:gui_grey, "", s:c_grey, "", "italic", "")
call s:hi("Conditional", s:gui_red, "", s:c_red, "", "bold", "")
call s:hi("Constant", s:gui_green, "", s:c_green, "", "", "")
call s:hi("Define", s:gui_fg, "", "NONE", "", "", "")
call s:hi("Delimiter", s:gui_white, "", s:c_white, "", "", "")
call s:hi("Exception", s:gui_yellow, "", s:c_yellow, "", "", "")
call s:hi("Float", s:gui_magenta, "", s:c_magenta, "", "", "")
call s:hi("Function", s:gui_fg, "", "NONE", "", "", "")
call s:hi("Identifier", s:gui_green, "", s:c_green, "", "underline", "")
call s:hi("Include", s:gui_cyan, "", s:c_cyan, "", "underline", "")
call s:hi("Keyword", s:gui_blue, "", s:c_blue, "", "bold", "")
call s:hi("Label", s:gui_grey, "", s:c_grey, "", "bold", "")
call s:hi("Number", s:gui_num, "", s:c_num, "", "", "")
call s:hi("Operator", s:gui_grey, "", s:c_grey, "", "NONE", "")
call s:hi("PreProc", s:gui_blue, "", s:c_blue, "", "bold", "")
call s:hi("Repeat", s:gui_yellow, "", s:c_yellow, "", "bold", "")
call s:hi("Special", s:gui_red, "", s:c_red, "", "", "")
call s:hi("SpecialChar", s:gui_bright, "", s:c_bright, "", "", "")
call s:hi("SpecialComment", s:gui_cyan, "", s:c_cyan, "", "italic", "")
call s:hi("Statement", s:gui_blue, "", s:c_yellow, "", "bold", "")
call s:hi("StorageClass", s:gui_white, "", s:c_white, "", "", "")
call s:hi("String", s:gui_cyan, "", s:c_cyan, "", "", "")
call s:hi("Structure", s:gui_bright, "", s:c_bright, "", "", "")
call s:hi("Tag", s:gui_magenta, "", s:c_magenta, "", "", "")
call s:hi("Todo", s:gui_red, "NONE", s:c_red, "NONE", "", "")
call s:hi("Type", s:gui_yellow, "", s:c_yellow, "", "underline", "")
call s:hi("Typedef", s:gui_yellow, "", s:c_yellow, "", "underline", "")
hi! link Macro Define
hi! link PreCondit PreProc

"+-----------+
"+ Languages +
"+-----------+
call s:hi("awkCharClass", s:gui_white, "", s:c_white, "", "", "")
call s:hi("awkPatterns", s:gui_red, "", s:c_red, "", "bold", "")
hi! link awkArrayElement Identifier
hi! link awkBoolLogic Keyword
hi! link awkBrktRegExp SpecialChar
hi! link awkComma Delimiter
hi! link awkExpression Keyword
hi! link awkFieldVars Identifier
hi! link awkLineSkip Keyword
hi! link awkOperator Operator
hi! link awkRegExp SpecialChar
hi! link awkSearch Keyword
hi! link awkSemicolon Delimiter
hi! link awkSpecialCharacter SpecialChar
hi! link awkSpecialPrintf SpecialChar
hi! link awkVariables Identifier

call s:hi("cIncluded", s:gui_magenta, "", s:c_magenta, "", "", "")
hi! link cOperator Operator
hi! link cPreCondit PreCondit

hi! link csPreCondit PreCondit
hi! link csType Type
hi! link csXmlTag SpecialComment

call s:hi("cssAttributeSelector", s:gui_yellow, "", s:c_yellow, "", "", "")
call s:hi("cssDefinition", s:gui_yellow, "", s:c_yellow, "", "NONE", "")
call s:hi("cssIdentifier", s:gui_blue, "", s:c_yellow, "", "underline", "")
call s:hi("cssStringQ", s:gui_yellow, "", s:c_yellow, "", "", "")
hi! link cssAttr Keyword
hi! link cssBraces Delimiter
hi! link cssClassName cssDefinition
hi! link cssColor Number
hi! link cssProp cssDefinition
hi! link cssPseudoClass cssDefinition
hi! link cssPseudoClassId cssPseudoClass
hi! link cssVendor Keyword

call s:hi("dosiniHeader", s:gui_grey, "", s:c_grey, "", "", "")
hi! link dosiniLabel Type

call s:hi("dtBooleanKey", s:gui_yellow, "", s:c_yellow, "", "", "")
call s:hi("dtExecKey", s:gui_yellow, "", s:c_yellow, "", "", "")
call s:hi("dtLocaleKey", s:gui_yellow, "", s:c_yellow, "", "", "")
call s:hi("dtNumericKey", s:gui_yellow, "", s:c_yellow, "", "", "")
call s:hi("dtTypeKey", s:gui_yellow, "", s:c_yellow, "", "", "")
hi! link dtDelim Delimiter
hi! link dtLocaleValue Keyword
hi! link dtTypeValue Keyword

call s:hi("DiffAdd", s:gui_diff, s:gui_bg2, s:c_diff, s:c_bg, "", "")
call s:hi("DiffChange", s:gui_num, s:gui_bg2, s:c_num, s:c_bg, "", "")
call s:hi("DiffDelete", s:gui_err, s:gui_bg2, s:c_err, s:c_bg, "", "")
call s:hi("DiffText", s:gui_magenta, s:gui_bg2, s:c_magenta, s:c_bg, "", "")

"" Legacy groups for official git.vim and diff.vim syntax
hi! link diffAdded DiffAdd
hi! link diffChanged DiffChange
hi! link diffRemoved DiffDelete

call s:hi("gitconfigVariable", s:gui_yellow, "", s:c_yellow, "", "", "")

call s:hi("goBuiltins", s:gui_yellow, "", s:c_yellow, "", "", "")
hi! link goConstants Keyword

call s:hi("htmlArg", s:gui_yellow, "", s:c_yellow, "", "", "")
call s:hi("htmlLink", s:gui_bright, "", "", "", "underline", "NONE")
hi! link htmlBold Bold
hi! link htmlEndTag htmlTag
hi! link htmlItalic Italic
hi! link htmlH1 markdownH1
hi! link htmlH2 markdownH1
hi! link htmlH3 markdownH1
hi! link htmlH4 markdownH1
hi! link htmlH5 markdownH1
hi! link htmlH6 markdownH1
hi! link htmlSpecialChar SpecialChar
hi! link htmlTag Keyword
hi! link htmlTagN htmlTag

call s:hi("javaDocTags", s:gui_yellow, "", s:c_yellow, "", "", "")
hi! link javaCommentTitle Comment
hi! link javaScriptBraces Delimiter
hi! link javaScriptIdentifier Keyword
hi! link javaScriptNumber Number

call s:hi("jsonKeyword", s:gui_yellow, "", s:c_yellow, "", "bold", "")

call s:hi("lessClass", s:gui_yellow, "", s:c_yellow, "", "bold", "")
hi! link lessAmpersand Keyword
hi! link lessCssAttribute Delimiter
hi! link lessFunction Function
hi! link cssSelectorOp Keyword

hi! link lispAtomBarSymbol SpecialChar
hi! link lispAtomList SpecialChar
hi! link lispAtomMark Keyword
hi! link lispBarSymbol SpecialChar
hi! link lispFunc Function

hi! link luaFunc Function

call s:hi("markdownBlockquote", s:gui_yellow, "", s:c_yellow, "", "", "")
call s:hi("markdownCode", s:gui_yellow, "", s:c_yellow, "", "", "")
call s:hi("markdownCodeDelimiter", s:gui_yellow, "", s:c_yellow, "", "", "")
call s:hi("markdownFootnote", s:gui_yellow, "", s:c_yellow, "", "", "")
call s:hi("markdownId", s:gui_yellow, "", s:c_yellow, "", "", "")
call s:hi("markdownIdDeclaration", s:gui_yellow, "", s:c_yellow, "", "", "")
call s:hi("markdownH1", s:gui_grey, "", s:c_grey, "", "", "")
call s:hi("markdownLinkText", s:gui_blue, "", s:c_blue, "", "", "")
call s:hi("markdownUrl", s:gui_blue, "", s:c_blue, "", "NONE", "")
hi! link markdownFootnoteDefinition markdownFootnote
hi! link markdownH2 markdownH1
hi! link markdownH3 markdownH1
hi! link markdownH4 markdownH1
hi! link markdownH5 markdownH1
hi! link markdownH6 markdownH1
hi! link markdownIdDelimiter Keyword
hi! link markdownLinkDelimiter Keyword
hi! link markdownLinkTextDelimiter Keyword
hi! link markdownListMarker Keyword
hi! link markdownRule Keyword
hi! link markdownHeadingDelimiter Keyword

call s:hi("perlPackageDecl", s:gui_yellow, "", s:c_yellow, "", "", "")

call s:hi("phpClasses", s:gui_yellow, "", s:c_yellow, "", "", "")
call s:hi("phpDocTags", s:gui_yellow, "", s:c_yellow, "", "", "")
hi! link phpDocCustomTags phpDocTags
hi! link phpMemberSelector Keyword

call s:hi("podCmdText", s:gui_yellow, "", s:c_yellow, "", "", "")
call s:hi("podVerbatimLine", s:gui_blue, "", "NONE", "", "", "")
hi! link podFormat Keyword

hi! link pythonBuiltin Type
hi! link pythonEscape SpecialChar

call s:hi("rubyConstant", s:gui_yellow, "", s:c_yellow, "", "", "")
call s:hi("rubySymbol", s:gui_cyan, "", s:c_cyan, "", "bold", "")
hi! link rubyAttribute Identifier
hi! link rubyBlockParameterList Operator
hi! link rubyInterpolationDelimiter Keyword
hi! link rubyKeywordAsMethod Function
hi! link rubyLocalVariableOrMethod Function
hi! link rubyPseudoVariable Keyword
hi! link rubyRegexp SpecialChar

call s:hi("sassClass", s:gui_yellow, "", s:c_yellow, "", "", "")
call s:hi("sassId", s:gui_yellow, "", s:c_yellow, "", "underline", "")
hi! link sassAmpersand Keyword
hi! link sassClassChar Delimiter
hi! link sassControl Keyword
hi! link sassControlLine Keyword
hi! link sassExtend Keyword
hi! link sassFor Keyword
hi! link sassFunctionDecl Keyword
hi! link sassFunctionName Function
hi! link sassidChar sassId
hi! link sassInclude SpecialChar
hi! link sassMixinName Function
hi! link sassMixing SpecialChar
hi! link sassReturn Keyword

hi! link shCmdParenRegion Delimiter
hi! link shCmdSubRegion Delimiter
hi! link shDerefSimple Identifier
hi! link shDerefVar Identifier

hi! link sqlKeyword Keyword
hi! link sqlSpecial Keyword

call s:hi("vimAugroup", s:gui_yellow, "", s:c_yellow, "", "", "")
call s:hi("vimMapRhs", s:gui_yellow, "", s:c_yellow, "", "", "")
call s:hi("vimNotation", s:gui_yellow, "", s:c_yellow, "", "", "")
hi! link vimFunc Function
hi! link vimFunction Function
hi! link vimUserFunc Function

call s:hi("xmlAttrib", s:gui_yellow, "", s:c_yellow, "", "", "")
call s:hi("xmlCdataStart", s:gui_green, "", s:c_green, "", "bold", "")
call s:hi("xmlNamespace", s:gui_yellow, "", s:c_yellow, "", "", "")
hi! link xmlAttribPunct Delimiter
hi! link xmlCdata Comment
hi! link xmlCdataCdata xmlCdataStart
hi! link xmlCdataEnd xmlCdataStart
hi! link xmlEndTag xmlTagName
hi! link xmlProcessingDelim Keyword
hi! link xmlTagName Keyword

call s:hi("yamlBlockMappingKey", s:gui_yellow, "", s:c_yellow, "", "", "")
hi! link yamlBool Keyword
hi! link yamlDocumentStart Keyword

"+----------------+
"+ Plugin Support +
"+----------------+
"+--- UI ---+
" ALE
" > w0rp/ale
call s:hi("ALEWarningSign", s:gui_num, "", s:c_num, "", "", "")
call s:hi("ALEErrorSign" , s:gui_err, "", s:c_err, "", "", "")

" GitGutter
" > airblade/vim-gitgutter
call s:hi("GitGutterAdd", s:gui_diff, "", s:c_diff, "", "", "")
call s:hi("GitGutterChange", s:gui_num, "", s:c_num, "", "", "")
call s:hi("GitGutterDelete", s:gui_err, "", s:c_err, "", "", "")

" NERDTree
" > scrooloose/nerdtree
call s:hi("NERDTreeExecFile", s:gui_yellow, "", s:c_yellow, "", "", "")
hi! link NERDTreeDirSlash Keyword
hi! link NERDTreeHelp Comment

" Sneak
call s:hi("Sneak", s:gui_bg, s:gui_num, s:c_bg, s:c_num, "bold", "")
call s:hi("SneakScope", s:gui_bg, s:gui_diff, s:c_bg, s:c_diff, "bold", "")

" vim-plug
" > junegunn/vim-plug
call s:hi("plugDeleted", s:gui_grey, "", "", s:c_grey, "", "")

let g:limelight_conceal_ctermfg=s:c_grey
let g:limelight_conceal_guifg=s:gui_grey

"+--- Languages ---+
" JavaScript
" > pangloss/vim-javascript
call s:hi("jsGlobalNodeObjects", s:gui_magenta, "", s:c_magenta, "", "italic", "")
hi! link jsBrackets Delimiter
hi! link jsFuncCall Function
hi! link jsFuncParens Delimiter
hi! link jsNoise Delimiter
hi! link jsPrototype Keyword
hi! link jsRegexpString SpecialChar

" Markdown
hi! link mkdBold Bold
hi! link mkdItalic Italic
hi! link mkdString Keyword
hi! link mkdCodeStart mkdCode
hi! link mkdCodeEnd mkdCode
hi! link mkdBlockquote Comment
hi! link mkdListItem Keyword
hi! link mkdListItemLine Normal
hi! link mkdFootnotes mkdFootnote
hi! link mkdLink markdownLinkText
hi! link mkdURL markdownUrl
hi! link mkdInlineURL mkdURL
hi! link mkdID Identifier "CHECK
hi! link mkdLinkDef mkdLink
hi! link mkdLinkDefTarget mkdURL
hi! link mkdLinkTitle mkdInlineURL
hi! link mkdDelimiter Keyword
