let g:colors_name = "tao"

hi Normal                  ctermfg=none
hi Boolean                 ctermfg=12
hi Comment                 ctermfg=8			 ctermbg=none    cterm=italic
hi Conditional             ctermfg=1
hi Constant                ctermfg=10                      cterm=italic
hi Cursor                  ctermfg=none    ctermbg=178     cterm=none
hi Debug                   ctermfg=13
hi Define                  ctermfg=none
hi Delimiter               ctermfg=7       ctermbg=none
hi DiffLine                ctermfg=3
hi DiffOldLine             ctermfg=13
hi DiffOldFile             ctermfg=13
hi DiffNewFile             ctermfg=2
hi DiffAdd                 ctermfg=2       ctermbg=none
hi DiffAdded               ctermfg=10
hi DiffDelete              ctermfg=1       ctermbg=none
hi DiffRemoved             ctermfg=9
hi DiffChange              ctermfg=3       ctermbg=none
hi DiffChanged             ctermfg=11
hi DiffText                ctermfg=15      ctermbg=4
hi Directory               ctermfg=12
hi Error                   ctermfg=198     ctermbg=none    cterm=bold
hi Exception               ctermfg=11
hi Float                   ctermfg=5
hi FoldColumn              ctermfg=6       ctermbg=none
hi Folded                  ctermfg=15      ctermbg=241
hi Function                ctermfg=none
hi Identifier              ctermfg=2                       cterm=bold
hi IncSearch               ctermfg=236     ctermbg=231
hi Keyword                 ctermfg=4											 cterm=bold
hi Label                   ctermfg=8
hi Macro                   ctermfg=1
hi MatchParen              ctermfg=11      ctermbg=none    cterm=bold
hi ModeMsg                 ctermfg=14
hi MoreMsg                 ctermfg=2
hi NonText                 ctermfg=14      ctermbg=none
hi Number                  ctermfg=214
hi Operator                ctermfg=8											 cterm=none
hi PreCondit               ctermfg=10										   cterm=none
hi PreProc                 ctermfg=4
hi Question                ctermfg=7
hi Repeat                  ctermfg=12
hi Search                  ctermfg=231     ctermbg=236
hi SpecialChar             ctermfg=15
hi SpecialComment          ctermfg=6
hi Special                 ctermfg=1
hi SpecialKey              ctermfg=13
hi Statement               ctermfg=4       ctermbg=none
hi StorageClass            ctermfg=7
hi String                  ctermfg=6
hi Structure               ctermfg=15
hi Tag                     ctermfg=5
hi Title                   ctermfg=6       ctermbg=none
hi Todo                    ctermfg=1       ctermbg=7
hi Typedef                 ctermfg=1       cterm=underline
hi Type                    ctermfg=13      cterm=underline
hi Underlined              ctermfg=14      ctermbg=none
hi VertSplit               ctermfg=15      ctermbg=none    cterm=none
hi Visual                  ctermfg=0       ctermbg=5
hi VisualNOS               ctermfg=0       ctermbg=12      cterm=bold
hi WarningMsg              ctermfg=213                     cterm=bold
hi WildMenu                ctermfg=236     ctermbg=12
hi CursorLine              ctermfg=none    ctermbg=none    cterm=bold
hi LineNr                  ctermfg=8                       cterm=none
hi CursorLineNr            ctermfg=12      ctermbg=none    cterm=none
hi ErrorMsg                ctermfg=1       ctermbg=none    cterm=bold

" ALE
hi ALEErrorSign            ctermfg=204     ctermbg=none		 cterm=bold
hi ALEWarningSign          ctermfg=172     ctermfg=none		 cterm=bold
" html
hi htmlTag                 ctermfg=3
hi htmlEndTag              ctermfg=11
hi htmlTagName             ctermfg=9

" xml
hi xmlTag                  ctermfg=3
hi xmlEndTag               ctermfg=11
hi xmlTagName              ctermfg=9

" NeoComplete
hi Pmenu                   ctermfg=14       ctermbg=236
hi PmenuSel                ctermfg=4       ctermbg=none
hi PmenuSbar               ctermfg=1       ctermbg=none

" statusline
hi User1          ctermfg=6  ctermbg=236 cterm=bold
hi User2          ctermfg=1  ctermbg=236 cterm=bold
hi User3          ctermfg=12 ctermbg=236 cterm=bold
hi User4          ctermfg=14 ctermbg=236 cterm=bold
hi User5          ctermfg=15 ctermbg=236 cterm=bold
hi User6          ctermfg=6  ctermbg=236 cterm=bold
hi User7          ctermfg=6  ctermbg=237 cterm=bold
hi StatusLine     ctermfg=6  ctermbg=236 cterm=bold
hi StatusLineNC   ctermfg=6  ctermbg=236   cterm=none

let g:limelight_conceal_ctermfg=10

" Tab Line
hi TabLineSel term=none cterm=none ctermfg=7 ctermbg=0
highlight! link TabLine StatusLineNC
highlight! link TabLineFill StatusLineNC
