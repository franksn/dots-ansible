#{{{ default env
## setup default dirs ##
XDG_CONFIG_HOME="$HOME/.config"
XDG_BIN_DIR="$HOME/bin"

export XDG_CONFIG_HOME XDG_BIN_DIR 

## environment variables ##
FZF_DEFAULT_COMMAND='ag -g ""'
FZF_DEFAULT_OPTS="--algo=v2 --color=bw"
EDITOR="vim"
LANG=en_US.UTF-8
MYSQL_HISTFILE=/dev/null

export FZF_DEFAULT_COMMAND FZF_DEFAULT_OPTS EDITOR LANG MYSQL_HISTFILE
#}}}
#{{{ PATHS
### PATH ###
PATH=$HOME/.fzf/bin:$XDG_BIN_DIR:$PATH

export PATH
#}}}
