#  ___
# | __|  _ _ _  __ ___
# | _| || | ' \/ _(_-<
# |_| \_,_|_||_\__/__/

#!/bin/bash

pubip() {
    curl -s ipinfo.io
}

mkcd () {
    if [ -d "$1" ] ; then
        cd "$1" || exit 1
        return
    fi
    mkdir -p "$1" && cd "$1" || exit 1
}

extract() {
    if [ -f $1 ] ; then
        case $1 in
            *.tar.bz2)   tar xvjf $1     ;;
            *.tar.gz)    tar xvzf $1     ;;
            *.bz2)       bunzip2 $1      ;;
            *.rar)       unrar x $1      ;;
            *.gz)        gunzip $1       ;;
            *.tar)       tar xvf $1      ;;
            *.tbz2)      tar xvjf $1     ;;
            *.tgz)       tar xvzf $1     ;;
            *.zip)       unzip $1        ;;
            *.Z)         uncompress $1   ;;
            *.7z)        7z x $1         ;;
            *)           echo "'$1' cannot be extracted via >extract<" ;;
        esac
    else
        echo "'$1' is not a valid file!"
    fi
}

compress () {
    if test -e "$1"; then
        if [ "$2" ]; then
            case $2 in
                tgz | tar.gz) tar -zcvf"$1.$2" "$1" ;;
                tbz2 | tar.bz2) tar -jcvf"$1.$2" "$1" ;;
                tar.Z) tar -Zcvf"$1.$2" "$1" ;;
                tar) tar -cvf"$1.$2" "$1" ;;
                zip) zip -r "$1.$2" "$1" ;;
                gz | gzip) gzip "$1" ;;
                bz2 | bzip2) bzip2 "$1" ;;
                gpg) gpg -e --default-recipient-self "$1" ;;
                *)
                echo "Error: $2 isn't a valid compression type"
                ;;
            esac
        else
            compress "$1" tar.gz
        fi
    else
        echo "File ('$1') does not exist"
    fi
}

gifify() {
    if [ -n "$1" ] && [ -n "$2" ] && [ -n "$3" ]; then
        mkdir gif-tmp
        ffmpeg -y -i "$1" -vf fps=30,scale="$3":-1:flags=lanczos,palettegen gif-tmp/"$1".png
        ffmpeg -i "$1" -i gif-tmp/"$1".png -filter_complex \
        "fps=30,scale="$3":-1:flags=lanczos[x];[x][1:v]paletteuse" "$2"
        rm -rf gif-tmp/
    else
        echo "proper usage: gif-ify <input_movie.mov> <output_file.gif>"
        echo "You DO need to include extensions."
    fi
}

up() {
    [ -z "$1" ] && cd .. && pwd && return 0
    for _ in $(seq 1 "$1"); do up=../$up; done
    cd "$up" || exit 1
    pwd
}

sprunge() {
    test -z "$1" && FILE='-' || FILE=$1
    curl -sF "sprunge=<${FILE}" http://sprunge.us
}

shorten() {
    if [ $# -gt 0 ]; then
        shorten=$(curl -F"shorten=$*" https://0x0.st)
        echo "$shorten"
    fi
}

upload() {
    if [ $# -gt 0 ]; then
        url=$(curl -F"file=@$*" https://0x0.st)
        echo "$url"
    fi
}

rensf ()
{
    if [ $# -lt 2 ]
    then
	puse "ren_all_suf oldsuffix newsuffix"
	return 1
    fi

    oldsuffix="${1}"
    newsuffix="${2}"

    # fake command to check if the suffix really exists
    if ! ls *."${oldsuffix}" 2>/dev/null
    then
	pwarn "There are no files with the suffix \`${oldsuffix}'."
	return 1
    fi

    for file in *."${oldsuffix}"
    do
	newname=$(printf "${file}\n" | sed "s/${oldsuffix}/${newsuffix}/")
	mv -i "${file}" "${newname}"
    done

    unset oldsuffix newsuffix newname
}

# rename all the files with a new prefix
# Arguments: $1 -> the old prefix
#            $2 -> the new prefix
renpf ()
{
    if [ $# -lt 2 ]
    then
	puse "ren_all_pref oldprefix newprefix"
	return 1
    fi

    oldprefix="${1}"
    newprefix="${2}"

    # fake command to check if the prefix really exists
    ls "${oldprefix}"* 2>/dev/null
    if ! ls *."${oldprefix}" 2>/dev/null
    then
	pwarn "There are no files with the prefix \`${oldprefix}'."
	return 1
    fi

    for file in "${oldprefix}"*
    do
	newname=$(printf "${file}\n" | sed "s/${oldprefix}/${newprefix}/")
	mv -i "${file}" "${newname}"
    done

    unset oldprefix newprefix newname
}

transfer() { 
    # check arguments
    if [ $# -eq 0 ]; 
    then 
        echo "No arguments specified. Usage:\necho transfer /tmp/test.md\ncat /tmp/test.md | transfer test.md"
        return 1
    fi
    # get temporarily filename, output is written to this file show progress can be showed
    tmpfile=$( mktemp -t transferXXX )
    # upload stdin or file
    file=$1

    if tty -s; 
    then 
        basefile=$(basename "$file" | sed -e 's/[^a-zA-Z0-9._-]/-/g') 
        if [ ! -e $file ];
        then
            echo "File $file doesn't exists."
            return 1
        fi
        if [ -d $file ];
        then
            # zip directory and transfer
            zipfile=$( mktemp -t transferXXX.zip )
            cd $(dirname $file) && zip -r -q - $(basename $file) >> $zipfile
            curl --progress-bar --upload-file "$zipfile" "https://transfer.sh/$basefile.zip" >> $tmpfile
            rm -f $zipfile
        else
            # transfer file
            curl --progress-bar --upload-file "$file" "https://transfer.sh/$basefile" >> $tmpfile
        fi
    else 
        # transfer pipe
        curl --progress-bar --upload-file "-" "https://transfer.sh/$file" >> $tmpfile
    fi
   
    # cat output link
    cat $tmpfile
    # cleanup
    rm -f $tmpfile
}
#}}}

